#ifndef SERENITY_CORE_HPP
#define SERENITY_CORE_HPP

#include <queue>
#include <bitset>
#include <vector>
#include <array>
#include <memory>

#define MAX_COMPONENT_AMOUNT 64

namespace serenity {

using entity_t = uint32_t;
using Entity = entity_t;
using ComponentID = std::size_t;

struct Component{};

inline ComponentID getUniqueComponentID() noexcept
{
    static ComponentID lastId(0);
    return lastId++;
}

template<class T>
inline ComponentID getComponentTypeID() noexcept
{
    static ComponentID typeID(getUniqueComponentID());
    return typeID;
}

} // namespace serenity

#endif // SERENITY_CORE_HPP
