#ifndef SERENITY_ENTITYCONTAINER_HPP
#define SERENITY_ENTITYCONTAINER_HPP

#include "Core.hpp"

namespace serenity {

struct EntityContainer
{

    /**
        As Entities are just numbers, we need a separate class to handle what an Entity could store,
        This way, moving the Entity won't move the Components and signature.
        This class is in fact a struct, but is not meant to be instanciated by the user.
        This is only used in serenity::World. DON'T INSTANCIATE THIS CLASS
    **/

    void kill();
    /** Destroy the Container, clears Signature and Component Array **/

    template<class T, class... C>
    void assign(C... constructor_arguments)
    /** Assigns a Component to the Container **/
    {
        componentSignature[getComponentTypeID<T>()] = true;
        componentArray[getComponentTypeID<T>()] = std::make_shared<T>(constructor_arguments...);
    }

    template<class T>
    T& get()
    /** Returns a reference to a Component of type 'T' **/
    /** TODO Throw a reference if the component is not assigned **/
    {
        return static_cast<T&>(*componentArray[getComponentTypeID<T>()]);
    }

    std::bitset<MAX_COMPONENT_AMOUNT> componentSignature;
    std::array<std::shared_ptr<Component>, MAX_COMPONENT_AMOUNT> componentArray;
};

} // namespace serenity

#endif // SERENITY_ENTITYCONTAINER_HPP
