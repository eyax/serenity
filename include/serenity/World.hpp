#ifndef SERENITY_WORLD_HPP
#define SERENITY_WORLD_HPP

#include "EntityContainer.hpp"
#include "System.hpp"

namespace serenity {

class World
{

    /**
        The Goal of the World class is to create and handle Entities and Systems
    **/

public:

        World();
        Entity createEntity();
        void destroyEntity(Entity entity);
        void refreshSystems();

        template<class T, class... C>
        void assign(Entity entity, C... constructor_arguments)
        {
            m_entityContainers[entity].assign<T, C...>(constructor_arguments...);
        }

        template<class T>
        T& get(Entity entity)
        {
            return m_entityContainers[entity].get<T>();
        }

        template<class T>
        bool isComponentOn(Entity entity)
        {
            return m_entityContainers[entity].componentSignature[getComponentTypeID<T>()];
        }

        template<class T, class... C>
        void addSystem(C... constructor_arguments)
        {
            m_systems.push_back(std::make_unique<T>(constructor_arguments...));
            m_systems[m_systems.size()-1]->setWorld(this);
        }

        void loop()
        {
            for(auto& system : m_systems)
            {
                system->loop();
            }
        }

    private:

        entity_t m_currentEntity;

        std::queue<entity_t> m_recycleQueue;
        std::vector<EntityContainer> m_entityContainers;
        std::vector<std::unique_ptr<SystemBase>> m_systems;
};

} // namespace serenity

#endif // SERENITY_WORLD_HPP
