#ifndef SERENITY_SYSTEM_HPP
#define SERENITY_SYSTEM_HPP

#include "Core.hpp"
#include <vector>

namespace serenity {

class World;

class SystemBase
{
public:
    std::bitset<MAX_COMPONENT_AMOUNT>& getSignature() {return m_signature;}
    std::vector<Entity> entities;
    World* world;

    virtual void loop() = 0;
    void setWorld(World* _world){world = _world;}

    virtual ~SystemBase()
    {

    }

protected:

    std::bitset<MAX_COMPONENT_AMOUNT> m_signature;

};

template<typename... Dependencies>
class System : public SystemBase
{
    public:
        System()
        {
            createSystemSignature<Dependencies...>();
        }

        virtual ~System()
        {

        }

    protected:

    private:

    template<typename First = void, typename... Rest>
    void createSystemSignature()
    {
        if(!std::is_void<First>())
        {
            m_signature[getComponentTypeID<First>()] = true;
            createSystemSignature<Rest...>();
        }
    }

};

} // namespace serenity

#endif // SERENITY_SYSTEM_HPP
