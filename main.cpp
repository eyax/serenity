#include <iostream>
#include "serenity/Serenity.hpp"

namespace sr = serenity;

struct Vector2
{
    float x, y;
    Vector2() : x(0), y(0) {}
    Vector2(float x, float y) : x(x), y(y) {}
    void operator+=(Vector2& other)
    {
        x += other.x;
        y += other.y;
    }
};

struct Transform : sr::Component
{
    Vector2 position, velocity;
    Transform() : velocity(2, 5) {}
};

struct Movement : sr::System<Transform> //This system will affect all entities with a Position and Velocity component, these are called dependencies
{
    void loop() override //Called manually from the World (world.loop())
    {
        for(const auto& entity : entities) //entities is a std::vector of all entities compatible with the dependencies
        {
            Transform& transform = world->get<Transform>(entity); //world is a pointer to the active world
            transform.position += transform.velocity;
        }
    }
};

int main()
{

    sr::World world;
    sr::Entity entity = world.createEntity(); //An Entity is just a number, to create it we need to ask to the World

    world.assign<Transform>(entity); //Assign a 'Position' component to 'entity'

    world.addSystem<Movement>(); //Adding the Movement system to the world
    world.refreshSystems(); //Refreshing each 'entities' vectors in the Systems (only call when needed!)

    Transform& transform = world.get<Transform>(entity); //get the 'Position' component from 'entity'

    while(true)
    {
        world.loop(); //Call each loop() methods from Systems
        std::cout << "{" << transform.position.x << ", " << transform.position.y << "}" << std::endl; //Drawing new transform state
    }

    return 0;
}
