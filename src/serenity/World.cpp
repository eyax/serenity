#include "serenity/World.hpp"

namespace serenity {

World::World() : m_currentEntity(0)
{

}

Entity World::createEntity()
{
    if(m_recycleQueue.empty())
    {
        m_entityContainers.push_back(EntityContainer());
        return m_currentEntity++;
    }

    Entity& r = m_recycleQueue.front();
    m_recycleQueue.pop();
    return r;
}

void World::destroyEntity(Entity entity) /**TODO avoid destroying an entity twice**/
{
    m_entityContainers[entity].kill();
    m_recycleQueue.push(entity);
}

void World::refreshSystems()
{
    for(auto& system : m_systems)
    {
        system->entities.clear();
        for(unsigned int entity = 0; entity < m_entityContainers.size(); ++entity)
        {
            auto res = system->getSignature() & m_entityContainers[entity].componentSignature;
            for(unsigned int i = 0; i < MAX_COMPONENT_AMOUNT; ++i)
            {
                if(res[i])
                {
                    system->entities.push_back(entity);
                }
            }
        }
    }
}

} // namespace serenity
