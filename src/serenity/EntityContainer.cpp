#include "serenity/EntityContainer.hpp"

namespace serenity {

void EntityContainer::kill()
{
    componentSignature.reset();
    componentArray.fill(nullptr);
}

} // namespace serenity
