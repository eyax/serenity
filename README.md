# Serenity, an Open Source C++11 Type-Safe Entity Component System [WIP!]
## Installation
Serenity  should work on any *ISO C++11*-compliant compiler. (Tested on **MinGW**, **MinGW64** and **clang**)

Most of the library is template-based, this way, the only given files are the header and source files, no need to compile it before!

You can change the *MAX_COMPONENT_AMOUNT* preprocessor macro and the entity_t definiton in the *Core.hpp* file (use uint32_t on 32bits system for performance).

Just include ***<serenity/Serenity.hpp>*** to have access to all the classes.

## Small tutorial

It's important to light up some definitions.

The goal of Serenity is to **dissociate Data from Logic**, to do that, 'objects' now have a different structure, they are called **entities**.

Entities, from a code point of view, are just numbers (uint32_t on 32bits systems, uint64_t on 64bits systems). They do not store anything.

To store data, you need to attach Components
**Components are just data**. Even if you can make custom constructors for them, it's an important design habit to **keep them as containers**.

Logic is handled using **Systems**

Systems are the **Behaviour** of entities, they have Component Dependencies, and affect at each 'loop' (called when you want) these components. ***They can also store data*** (but just for them), and can have custom constructors.

**Let's see some code:**

First of all, we need to create a World

    serenity::World world;
Then, let's create an entity

    serenity::Entity entity = world.createEntity();
It's important to create entities using the world, as they are only numbers and need to be unique in the context.

Let's add a component. In order to do that, we need to create one. And because we are ambitious, let's make two!

    struct Position : serenity::Component
    {
	    float x, y;
    }
    
    struct Velocity : serenity::Component
    {
	    float x, y;
	}

You can use you own classes inside the structure, it does not matter. Components can even be empty!

Back to our main code, we can assign those components to the entity we created before

    world.assign<Position>(entity);
    world.assign<Velocity>(entity);
Great. Now, logic!

To create logic, just create a struct derived from the System class

    struct Movement : serenity::System<Position, Velocity>
    {
	    ...
    }
The template arguments are the dependencies, this system will execute logic on every entity having the asked components

From inside the struct, you have access to multiple stuff:

- a `loop()` method to be overridden
- a `World* world` wich points to the world
- a `vector<serenity::Entity> entities` to access the entities with the asked dependencies

Let's complete our struct, you will also see how to retreive components from entities

    struct Movement : serenity::System<Position, Velocity>
    {
	    void loop() override
	    {
		    for(auto& entity : entities)
		    {
			    Position& position = world->get<Position>(entity); //Do NOT forget the & character! We do not want to copy the component
			    Velocity& velocity = world->get<Velocity>(entity);

				position.x += velocity.x; //Adding velocity to position in components
				position.y += velocity.y;
				std::cout << "{" << position.x << ", " << position.y << "}" << std::endl; //just to see some outputs
			}
		}
    }
Now, to get everything working, we need to complete our main code. Problem : We want to put values into our components, blank Velocity and Position values will cause runtime error!

To do that, change the component's constructor

    struct Position : serenity::Component
    {
	    float x, y;
	    Position() : x(0), y(0) {} //default constructor
	    Position(float x, float y) : x(x), y(y) {} //adding data with constructors
    }
    
    struct Velocity : serenity::Component
    {
	    float x, y;
	    Velocity(float x, float y) : x(x), y(y) {} //no default constructor!
	}
Where to call these constructors ? When assigning! Change the assigment part to this :

    world.assign<Position>(entity); //default constructor
    world.assign<Velocity>(entity, 3, 2); //constructor arguments in assign call
it's then time to inform the World about the system

    world.setSystem<Movement>(); //note that you can use the same constructor arguments withing this call
    world.refreshSystems(); //call this each time you need to refresh Systems 'entities' vector

then, implement a game loop. For this example we are just gonna make a simple infinite loop

    while(true)
    {
	    world.loop();
    }

You should get, at each frame, the position adding up to the velocity!
